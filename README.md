# README #

Joint RxPricing project

### What is new? ###

* Newest grails (3.3.3 at current time)
* Using gradle to build
* Using SiteMesh for generating pages
* Using database-migration to better make structual changes in database
* Still use Grails Quartz for running jobs
* Use CSS Grid for UI layout

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact